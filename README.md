# README #

01/11/2016

Edward Tsien

Merged over from private repo

* Basic text and image directory sharing site.
* Can create sub directories, as well as basic text input.
* Features include bootstrap-aided responsiveness and some in-line jQuery magic to keep things dynamic.
* Most CSS and html come stock.