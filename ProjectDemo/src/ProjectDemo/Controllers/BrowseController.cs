﻿using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDemo.Controllers
{
    public class BrowseController : Controller
    {
        public IActionResult Create()
        {
            return View("Index");
        }

        public IActionResult Box()
        {
            return View();
        }

        //public IActionResult Box(string inputs)
        //{
        //    if (inputs == "subdir1")
        //        return View("Sub");
        //    if (inputs == "text")
        //        return View("Txt");
        //    if (inputs == "img")
        //        return View("Img");
        //    else
        //        return View();
        //}

        public IActionResult Sub()
        {
            return View();
        }
        public IActionResult Txt()
        {
            return View();
        }
        public IActionResult Img()
        {
            return View();
        }
    }
}
